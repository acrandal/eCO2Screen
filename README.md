# eCO2 and TVOC indoor air quality screen

This is a simple little project to show a reasonable approximation of your indoor air quality. It uses a SGP30 sensor to get effective carbon dioxide (eCO2) and Total Volatile Organic Compounds (TVOC) output. The samples are rendered on a SSD1306 128x64 OLED screen for easy viewing in battery or wall powered situations.


